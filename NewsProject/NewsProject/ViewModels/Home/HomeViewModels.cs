﻿using NewsProject.BAL;
using PagedList;
using System.Linq;

namespace NewsProject.Models
{
    /// <summary>
    /// Модели для контроллеров HOME
    /// </summary>
    public class HomeViewModels
    {
        /// <summary>
        /// Модель для взаимодействия со страницей редактирования новостей
        /// </summary>
        public class EditViewModel
        {
            /// <summary>
            /// Объект класса Новость
            /// </summary>
            public NewsDto NewsToEdit { get; set; }
        }

        /// <summary>
        /// Модель для взаимодействия со страницей  отображения всех новостей
        /// </summary>
        public class NewsViewModel
        {
            /// <summary>
            /// Список новостей в зависимости от страницы
            /// </summary>
            public IPagedList<NewsDto> NewsPagesList { get; set; }            
        }

        /// <summary>
        /// Модель для взаимодействия со страницей добавления новостей
        /// </summary>
        public class AddNewsViewModel
        {
            /// <summary>
            /// Объект новой новости
            /// </summary>
            public NewsDto NewNews { get; set; }
        }

        /// <summary>
        /// Модель для взаимодействия со страницей "подробнее"
        /// </summary>
        public class MoreViewModel
        {
            /// <summary>
            /// Объект новости
            /// </summary>
            public NewsDto News { get; set; }

            /// <summary>
            /// Комментарии к новости
            /// </summary>
            public IOrderedEnumerable<CommentsDto> Comments { get; set; }

            /// <summary>
            /// Новый комментарий
            /// </summary>
            public CommentsDto NewComment { get; set; }            
        }
    }
}