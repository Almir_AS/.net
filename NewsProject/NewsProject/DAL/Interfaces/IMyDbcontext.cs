﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace NewsProject.DAL.Interfaces
{
    /// <summary>
    /// Интерфейс для контекста данных
    /// </summary>
    public interface IMyDbcontext
    {
        DbSet<Comments> Comments { get; set; }
        DbSet<News> News { get; set; }
        void Dispose();
        int SaveChanges();
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
