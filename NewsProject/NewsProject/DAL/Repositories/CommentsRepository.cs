﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NewsProject.DAL.Interfaces
{
    /// <summary>
    /// Репозиторий для комментариев
    /// </summary>
    public class CommentsRepository : IRepository<Comments>
    {
        private readonly IMyDbcontext _db;
        private bool _disposed = false;

        /// <summary>
        /// Конструктор принимает IMyDbcontext для реализации IoC
        /// </summary>
        /// <param name="context">контекст данных</param>
        public CommentsRepository(IMyDbcontext context)
        {
            _db = context;
        }

        /// <summary>
        /// Добавление нового комментария
        /// </summary>
        /// <param name="item">объект комментария</param>
        public void Create(Comments item)
        {
            _db.Comments.Add(item);
        }

        /// <summary>
        /// Удаление комментария
        /// </summary>
        /// <param name="id">ID комментария</param>
        public void Delete(int id)
        {
            var comment = _db.Comments.Find(id);
            if (comment != null)
                _db.Comments.Remove(comment);
        }

        /// <summary>
        /// Переопределенный метод для Dispose
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Ищет комментарий по заданному условию
        /// </summary>
        /// <param name="predicate">условие</param>
        /// <returns></returns>
        public IEnumerable<Comments> Find(Func<Comments, bool> predicate)
        {
            return _db.Comments.Where(predicate).ToList();
        }

        /// <summary>
        /// Возвращает комментарий по его ID
        /// </summary>
        /// <param name="id">ID комментария</param>
        /// <returns>комментарий</returns>
        public Comments Get(int id)
        {
            return _db.Comments.Find(id);
        }

        /// <summary>
        /// Возвращает IEnumerable всех комментаиев из БД
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Comments> GetAll()
        {
            return _db.Comments;
        }

        /// <summary>
        /// Сохраняет изменения контекста
        /// </summary>
        public void Save()
        {
            _db.SaveChanges();
        }

        /// <summary>
        /// Изменяет запись в БД
        /// </summary>
        /// <param name="item">объект комментария</param>
        public void Update(Comments item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }
    }
}