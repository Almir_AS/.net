﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewsProject.Startup))]
namespace NewsProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
