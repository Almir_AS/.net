﻿using NewsProject.BAL;
using NewsProject.BAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using static NewsProject.Models.HomeViewModels;
using PagedList;

namespace NewsProject.Controllers
{
    /// <summary>
    /// Основной контроллер
    /// </summary>
    [HandleError]
    public class HomeController : Controller
    {
        private readonly INewsService _newsService;
        private readonly ICommentsService _commentsService;

        /// <summary>
        /// Конструктор обеспечивает реализацию IoC для INewsService и ICommentsService
        /// </summary>
        /// <param name="newsService">сервис для новостей</param>
        /// <param name="commentsService">сервис для комментариев</param>
        public HomeController(INewsService newsService, ICommentsService commentsService)
        {
            this._newsService = newsService;
            this._commentsService = commentsService;
        }

        #region Index
        /// <summary>
        /// Главная страница отображает все новости (постранично)
        /// </summary>
        /// <param name="page">номер страницы</param>
        /// <returns>View</returns>
        public ActionResult Index(int? page)
        {
            var listOfNews = _newsService.GetAllNews().OrderByDescending(c => c.CreationDate);
            var pagedNewsList = listOfNews.ToPagedList((page ?? 1), 10);

            NewsViewModel model = new NewsViewModel()
            {
                NewsPagesList = pagedNewsList
            };

            return View(model);
        }
        #endregion

        #region More

        /// <summary>
        /// Контроллер для запросов Get страницы "Подробнее"
        /// </summary>
        /// <param name="id">ID новости</param>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult More(int id)
        {
            MoreViewModel model = new MoreViewModel()
            {
                Comments = _newsService.GetNewsComments(id).OrderBy(c => c.CreationDate),
                News = _newsService.FindNews(id)

            };
            return View(model);
        }

        /// <summary>
        /// Контроллер запросов Post для страницы "Подробнее"
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult More(MoreViewModel model)
        {
            model.NewComment.CreationDate = DateTime.Now;

            if (ModelState.IsValid)
            {
                _commentsService.AddComment(model.NewComment);
                return RedirectToAction("More");
            }

            MoreViewModel modelToSend = new MoreViewModel()
            {
                Comments = _newsService.GetNewsComments(model.NewComment.NewsId).OrderBy(c => c.CreationDate),
                News = _newsService.FindNews(model.NewComment.NewsId)
            };
            if (modelToSend.News == null) return HttpNotFound();
            return View(modelToSend);
        }
        #endregion        

        #region EditNews

        /// <summary>
        /// Контроллер запросов Get для страницы редактирвания новости
        /// </summary>
        /// <param name="id">ID новости</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            EditViewModel model = new EditViewModel() { NewsToEdit = _newsService.FindNews(id) };
            if (model.NewsToEdit == null) return HttpNotFound();
            return View(model);
        }

        /// <summary>
        /// Контроллер запросов Post для страницы редактирвания новости
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(EditViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.NewsToEdit.Comments = new List<CommentsDto>();
                _newsService.UpdateNews(model.NewsToEdit);
                return RedirectToAction("Index", "Home");
            }
            return View(model);

        }
        #endregion

        #region NewNews
        /// <summary>
        /// Контроллер запросов Get для страницы добавления новости
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddNews()
        {
            return View();
        }

        /// <summary>
        /// Контроллер запросов Post для страницы добавления новости
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddNews(AddNewsViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.NewNews.CreationDate = DateTime.Now;
                model.NewNews.Comments = new List<CommentsDto>();
                _newsService.AddNews(model.NewNews);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
        #endregion

        #region OtherViews
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        #endregion
    }
}