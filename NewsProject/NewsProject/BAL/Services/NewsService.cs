﻿using NewsProject.BAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using NewsProject.DAL;
using NewsProject.DAL.Interfaces;

namespace NewsProject.BAL.Services
{
    /// <summary>
    /// Класс обеспечивает работу с новостями (сокрывает взаимодействие с уровнем DAL)
    /// </summary>
    public class NewsService : INewsService
    {
        private IMyDbcontext Db { get; set; }
        private IRepository<News> RepoNews { get; set; }

        /// <summary>
        /// Конструктор обеспечивает IoC для создания контекста данных и репозитория новостей
        /// </summary>
        /// <param name="db">контекст данных</param>
        /// <param name="repoNewsNews">репозиторий новостей</param>
        public NewsService(IMyDbcontext db, IRepository<News> repoNewsNews)
        {
            Db = db;
            RepoNews = repoNewsNews;
        }

        /// <summary>
        /// Получает список всех новостей
        /// </summary>
        /// <returns> список всех новостей </returns>
        public List<NewsDto> GetAllNews()
        {      
            return RepoNews.GetAll().ToListNewsDto();
        }

        /// <summary>
        /// Добавляет новую новость 
        /// </summary>
        /// <param name="item">объект новости</param>
        public void AddNews(NewsDto item)
        {         
            RepoNews.Create(item.MapNewsDtoToNews());
            RepoNews.Save();
        }       

        /// <summary>
        /// Находит новость по ID
        /// </summary>
        /// <param name="id">ID новости</param>
        /// <returns>новость</returns>
        public NewsDto FindNews(int id)
        {         
            var newsFind = RepoNews.Find(c => c.Id == id).FirstOrDefault();
            return newsFind.MapNewsToNewsDto();
        }

        /// <summary>
        /// Возвращает коллекцию комментариев относящихся к данной новости
        /// </summary>
        /// <param name="id">ID новости</param>
        /// <returns>коллекция комментариев ICollection</returns>
        public ICollection<CommentsDto> GetNewsComments(int id)
        {        
            var result = RepoNews.Find(c => c.Id == id).FirstOrDefault();
            return result?.MapNewsToNewsDto().Comments;
        }

        /// <summary>
        /// Обновляет данные новости
        /// </summary>
        /// <param name="item">объект новости</param>
        public void UpdateNews(NewsDto item)
        {        
            var news = item.MapNewsDtoToNews();
            var itemToupdate = RepoNews.Find(c => c.Id == news.Id).FirstOrDefault();
            if (itemToupdate == null) return;
            itemToupdate.Body = news.Body;
            itemToupdate.Annotation = news.Annotation;
            itemToupdate.CreationDate = news.CreationDate;
            itemToupdate.Head = news.Head;
            itemToupdate.Author = news.Author;
            RepoNews.Update(itemToupdate);
            RepoNews.Save();
        }

        /// <summary>
        /// Удаляет новость 
        /// </summary>
        /// <param name="item">новость для удаления</param>
        public void DeleteNews(NewsDto item)
        {          
            var news = item.MapNewsDtoToNews();            
            RepoNews.Delete(RepoNews.Find(c => c.Id == news.Id).Select(c => c.Id).First());           
            RepoNews.Save();
        }
    }   
}