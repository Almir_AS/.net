﻿using NewsProject.DAL;
using System.Collections.Generic;
using System.Linq;

namespace NewsProject.BAL.Services
{
    public static class NewsServiceHelper
    {
        public static List<NewsDto> ToListNewsDto(this IEnumerable<News> newsList)
        {
            var newsDto = new List<NewsDto>();
            foreach (var c in newsList)
            {
                var temp = c.Comments.Select(u => new CommentsDto
                {
                    AuthorName = u.AuthorName,
                    Body = u.Body,
                    CreationDate = u.CreationDate,
                    Id = u.Id,
                    NewsId = u.NewsId
                }).ToList();

                newsDto.Add(new NewsDto
                {
                    Annotation = c.Annotation,
                    Body = c.Body,
                    Comments = temp,
                    CreationDate = c.CreationDate,
                    Head = c.Head,
                    Id = c.Id,
                    Author = c.Author
                });
            }
            return newsDto;
        }

        public static NewsDto MapNewsToNewsDto(this News news)
        {
            var temp = news.Comments.Select(u => new CommentsDto
            {
                AuthorName = u.AuthorName,
                Body = u.Body,
                CreationDate = u.CreationDate,
                Id = u.Id,
                NewsId = u.NewsId
            }).ToList();
            return new NewsDto
            {
                Annotation = news.Annotation,
                Body = news.Body,
                Comments = temp,
                CreationDate = news.CreationDate,
                Head = news.Head,
                Id = news.Id,
                Author = news.Author
            };
        }

        public static News MapNewsDtoToNews(this NewsDto newsDto)
        {
            var temp = newsDto.Comments.Select(u => new Comments
            {
                AuthorName = u.AuthorName,
                Body = u.Body,
                CreationDate = u.CreationDate,
                Id = u.Id,
                NewsId = u.NewsId
            }).ToList();
            return new News
            {
                Annotation = newsDto.Annotation,
                Body = newsDto.Body,
                Comments = temp,
                CreationDate = newsDto.CreationDate,
                Head = newsDto.Head,
                Id = newsDto.Id,
                Author = newsDto.Author
            };
        }
    }
}