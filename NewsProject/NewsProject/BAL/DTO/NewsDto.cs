﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NewsProject.BAL
{
    /// <summary>
    /// Класс новостей для передачи между уровнем View и Controller (используется в моделях передачи данных в View)
    /// </summary>
    public class NewsDto
    {
        [Required]      
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Поле {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [Display(Name = "Заголовок")]
        public string Head { get; set; }

        [Required]
        [StringLength(5000, ErrorMessage = "Поле {0} должно содержать не менее {2} символов.", MinimumLength = 10)]
        [Display(Name = "Содержание")]
        public string Body { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Дата создания")]
        public DateTime CreationDate { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Поле {0} должно содержать не менее {2} символов.", MinimumLength = 10)]
        [Display(Name = "Краткое содержание")]
        public string Annotation { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Поле {0} должно содержать не менее {2} символов.", MinimumLength = 1)]
        [Display(Name = "Автор")]
        public string Author { get; set; }

        public virtual ICollection<CommentsDto> Comments { get; set; }
    }
}