﻿using System.Collections.Generic;

namespace NewsProject.BAL.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса работы с новостями (используется в контроллерах)
    /// </summary>
    public interface INewsService
    {
        void AddNews(NewsDto item);        
        void UpdateNews(NewsDto item);
        NewsDto FindNews(int id);
        ICollection<CommentsDto> GetNewsComments(int id);
        List<NewsDto> GetAllNews();     
        void DeleteNews(NewsDto item);
    }
}
