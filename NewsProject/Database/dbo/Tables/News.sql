﻿CREATE TABLE [dbo].[News]
(
	[Id] INT  IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [Head] NVARCHAR(255) NOT NULL, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL, 
    [Annotation] NTEXT NOT NULL, 
    [Author] NVARCHAR(30) NOT NULL
)
