﻿/*
Скрипт развертывания для master

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "master"
:setvar DefaultFilePrefix "master"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Выполняется создание [dbo].[Comments]...';


GO
CREATE TABLE [dbo].[Comments] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Body]         NTEXT          NOT NULL,
    [CreationDate] NVARCHAR (255) NOT NULL,
    [NewsId]       INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[News]...';


GO
CREATE TABLE [dbo].[News] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Head]         NVARCHAR (255) NOT NULL,
    [Body]         NTEXT          NOT NULL,
    [CreationDate] DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[FK_Comments_ToTable_News]...';


GO
ALTER TABLE [dbo].[Comments] WITH NOCHECK
    ADD CONSTRAINT [FK_Comments_ToTable_News] FOREIGN KEY ([NewsId]) REFERENCES [dbo].[News] ([Id]);


GO
/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET IDENTITY_INSERT dbo.News ON
DECLARE @News TABLE(
	[Id] INT  NOT NULL PRIMARY KEY, 
    [Head] NVARCHAR(255) NOT NULL, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL
)


INSERT INTO @News ([Id], [Head], [Body], [CreationDate]) VALUES
(1,N'новость 1',N'текст новости 1',2016-09-11),
(2,N'новость 2',N'текст новости 2',2016-10-11),
(3,N'новость 3',N'текст новости 3',2016-05-11)



MERGE INTO [News] AS [target]
USING @News AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Head]	= [source].[Head],
			[target].[Body] = [source].[Body],
			[target].[CreationDate] = [source].[CreationDate]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Head], [Body], [CreationDate]) 
	VALUES ([source].[Id], [source].[Head], [source].[Body],[source].[CreationDate]);

SET IDENTITY_INSERT dbo.News OFF
GO


SET IDENTITY_INSERT dbo.Comments ON
DECLARE @Comments TABLE(
	[Id] INT   NOT NULL PRIMARY KEY, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] NVARCHAR(255) NOT NULL, 
    [NewsId] INT NOT NULL
)


INSERT INTO @Comments ([Id], [Body], [CreationDate], [NewsId]) VALUES
(1,N'Спасибо очень познавательно',2016-09-11, 1),
(2,N'так и не понял что хотел сказать автор',2016-11-15, 1),
(3,N'ну и ну',2016-09-11, 2),
(4,N'да видимо это очень крутая новость',2016-09-11, 2),
(5,N'Куда катится мир',2016-09-11, 3)



MERGE INTO [Comments] AS [target]
USING @News AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Body]	= [source].[Body],
			[target].[CreationDate] = [source].[CreationDate],
			[target].[NewsId] = [source].[NewsId]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Body], [CreationDate], [NewsId]) 
	VALUES ([source].[Id], [source].[Body], [source].[CreationDate],[source].[NewsId]);

SET IDENTITY_INSERT dbo.Comments OFF
GO


GO

GO
PRINT N'Существующие данные проверяются относительно вновь созданных ограничений';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[Comments] WITH CHECK CHECK CONSTRAINT [FK_Comments_ToTable_News];


GO
PRINT N'Обновление завершено.';


GO
