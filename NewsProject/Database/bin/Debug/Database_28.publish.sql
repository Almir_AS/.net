﻿/*
Скрипт развертывания для 555

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "555"
:setvar DefaultFilePrefix "555"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [master];


GO

IF (DB_ID(N'$(DatabaseName)') IS NOT NULL) 
BEGIN
    ALTER DATABASE [$(DatabaseName)]
    SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$(DatabaseName)];
END

GO
PRINT N'Выполняется создание $(DatabaseName)...'
GO
CREATE DATABASE [$(DatabaseName)]
    ON 
    PRIMARY(NAME = [$(DatabaseName)], FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix)_Primary.mdf')
    LOG ON (NAME = [$(DatabaseName)_log], FILENAME = N'$(DefaultLogPath)$(DefaultFilePrefix)_Primary.ldf') COLLATE SQL_Latin1_General_CP1_CI_AS
GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL,
                RECOVERY FULL,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CLOSE OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET READ_COMMITTED_SNAPSHOT OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                PAGE_VERIFY NONE,
                DATE_CORRELATION_OPTIMIZATION OFF,
                DISABLE_BROKER,
                PARAMETERIZATION SIMPLE,
                SUPPLEMENTAL_LOGGING OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET TRUSTWORTHY OFF,
        DB_CHAINING OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'Параметры базы данных изменить нельзя. Применить эти параметры может только пользователь SysAdmin.';
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET HONOR_BROKER_PRIORITY OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'Параметры базы данных изменить нельзя. Применить эти параметры может только пользователь SysAdmin.';
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET FILESTREAM(NON_TRANSACTED_ACCESS = OFF),
                CONTAINMENT = NONE 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF),
                MEMORY_OPTIMIZED_ELEVATE_TO_SNAPSHOT = OFF,
                DELAYED_DURABILITY = DISABLED 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)];


GO
IF fulltextserviceproperty(N'IsFulltextInstalled') = 1
    EXECUTE sp_fulltext_database 'enable';


GO
PRINT N'Выполняется создание [dbo].[Comments]...';


GO
CREATE TABLE [dbo].[Comments] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Body]         NTEXT          NOT NULL,
    [CreationDate] NVARCHAR (255) NOT NULL,
    [NewsId]       INT            NOT NULL,
    [AuthorName]   NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[News]...';


GO
CREATE TABLE [dbo].[News] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Head]         NVARCHAR (255) NOT NULL,
    [Body]         NTEXT          NOT NULL,
    [CreationDate] DATETIME       NOT NULL,
    [Annotation]   NTEXT          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[FK_Comments_ToTable_News]...';


GO
ALTER TABLE [dbo].[Comments]
    ADD CONSTRAINT [FK_Comments_ToTable_News] FOREIGN KEY ([NewsId]) REFERENCES [dbo].[News] ([Id]);


GO
-- Выполняется этап рефакторинга для обновления развернутых журналов транзакций на целевом сервере

IF OBJECT_ID(N'dbo.__RefactorLog') IS NULL
BEGIN
    CREATE TABLE [dbo].[__RefactorLog] (OperationKey UNIQUEIDENTIFIER NOT NULL PRIMARY KEY)
    EXEC sp_addextendedproperty N'microsoft_database_tools_support', N'refactoring log', N'schema', N'dbo', N'table', N'__RefactorLog'
END
GO
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = 'b8bd729d-fce4-46fc-8948-c0937ae8d83b')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('b8bd729d-fce4-46fc-8948-c0937ae8d83b')

GO

GO
/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
SET IDENTITY_INSERT dbo.News ON
DECLARE @News TABLE(
	[Id] INT  NOT NULL PRIMARY KEY, 
    [Head] NVARCHAR(255) NOT NULL, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL,
	[Annotation] NTEXT NULL
)


INSERT INTO @News ([Id], [Head], [Body], [CreationDate],[Annotation]) VALUES
(1,N'Валентина Терешкова открыла первую космическую школу Тольятти',N'Сегодня, 11 сентября, несмотря на воскресный день, почти все ученики школы №49, которая находится в 6-м квартале Тольятти, пришли на школьный двор в парадной форме. Этому сопутствовали несколько причин. Школа в этом году празднует свое 40-летие, а сегодня она открывает новую страницу своей истории.В присутствии первой в мире женщины-космонавта, совершившей космический полет в одиночку, Героя Советского Союза, Героя Социалистического труда нескольких стран, генерала-майора, депутата Государственной Думы ФС РФ Валентины Терешковой состоялось торжественное открытие мемориала в честь основателя российской космонавтики Сергея Королева. С этого дня школа также будет носить имя прославленного советского конструктора','2016-01-03',N'Валентина Терешкова открыла космическую школу в Тольятти'),
(2,N'Космический телескоп "Кеплер" попал в беду в 120 миллионах километров от Земли',N'Космический телескоп НАСА "Кеплер" попал в беду - находясь очень, очень далеко от дома.
Этот телескоп, который осуществляет поиски обитаемых планет в далеких областях космоса, перешел в аварийный режим, находясь на расстоянии 120 миллионов километров от нашей планеты, заставляя инженеров на Земле срочно искать возможные пути выхода из этой опасной ситуации.
Инженеры миссии обнаружили, что космический телескоп неисправен, во время последнего контакта связи с аппаратом, состоявшегося в четверг. Находясь в аварийном режиме, "Кеплер" сжигает повышенное количество топлива, "бросая все силы" на восстановление после сбоя.
"Аварийный режим характеризуется минимальной операционной активностью и повышенным расходом горючего", сообщает НАСА.
Ремонт телескопа, находящегося на столь большом расстоянии от нашей планеты, представляет значительную трудность. Сигналы, посылаемые телескопу "Кеплер" с нашей планеты, достигают его не сразу, что замедляет управление спутником при помощи команд с Земли.
"Даже при том, что сигналы представляют собой электромагнитные волны, которые движутся со скоростью света, тем не менее они идут до космического аппарата и обратно целых 13 минут", указывает НАСА в этом же сообщении.
"Охотник за планетами", космический телескоп НАСА "Кеплер", запущенный в 2009 г., завершил свою основную миссию в 2012 г., открыв примерно 5000 внесолнечных планет-кандидатов. Более одной тысячи из этих планет-кандидатов уже были подтверждены как планеты по информации, предоставленной НАСА. Два года назад началась дополнительная миссия телескопа "Кеплер". В рамках этой миссии "Кеплер" продолжает поиск экзопланет, а также производит поиск молодых звезд и других астрономических объектов.','2016-02-02',N'у космического телескопа Kepler возникли неполадки, ученые пытаются восстановить работу системы'),
(3,N'Microsoft Translator устраняет языковой барьер в межличностном общении',N'Технология машинного перевода при личном общении, разработанная командой машинного перевода исследовательского центра Microsoft в Редмонде, штат Вашингтон, может также помочь путешественникам общаться за рубежом с сотрудниками службы приема в отелях, с водителями такси и экскурсоводами в музеях. Учителя могут использовать их, чтобы вести диалог с родителями учеников, разговаривающими на других языках.
Во время проведения пилотного проекта в Нью-Йорке технология Microsoft помогала людям, не знающим английского, подавать документы на получение государственных удостоверений личности.
«Нашей конечной целью является полное устранение языковых барьеров», – сказал Оливье Фонтана, директор по продуктовой стратегии Microsoft Translator.
Эта технология применима для общения один на один, например, в случае, когда путешественнику нужно узнать информацию у консьержа, для выступления одного человека перед аудиторией, когда гид проводит экскурсию для большой группы туристов, и для разговора многих участников, например, в ситуации, когда иммигранты делятся опытом в группах поддержки.','2016-03-01',N'сотрудники Детского общества начали использовать функцию синхронного перевода в Microsoft Translator')



MERGE INTO [News] AS [target]
USING @News AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Head]	= [source].[Head],
			[target].[Body] = [source].[Body],
			[target].[CreationDate] = [source].[CreationDate],
			[target].[Annotation] = [source].[Annotation]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Head], [Body], [CreationDate],[Annotation]) 
	VALUES ([source].[Id], [source].[Head], [source].[Body],[source].[CreationDate],[source].[Annotation]);

SET IDENTITY_INSERT dbo.News OFF
GO


SET IDENTITY_INSERT dbo.Comments ON
DECLARE @Comments TABLE(
	[Id] INT   NOT NULL PRIMARY KEY, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] NVARCHAR(255) NOT NULL, 
    [NewsId] INT NOT NULL,
	[AuthorName] NVARCHAR(100) NOT NULL
)


INSERT INTO @Comments ([Id], [Body], [CreationDate], [NewsId],[AuthorName]) VALUES
(1,N'Спасибо очень познавательно','2016-01-03', 1, N'Николай Басков'),
(2,N'так и не понял что хотел сказать автор','2016-01-03', 1, N'Альберт Эйнштейн'),
(3,N'ну и ну','2016-01-03', 2, N'Виктор Франкл'),
(4,N'да видимо это очень крутая новость','2016-01-03', 2, N'Филлип Киркоров'),
(5,N'Куда катится мир','2016-01-03', 3, N'Владимир Владимирович')



MERGE INTO [Comments] AS [target]
USING @Comments AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Body]	= [source].[Body],
			[target].[CreationDate] = [source].[CreationDate],
			[target].[NewsId] = [source].[NewsId],
			[target].[AuthorName] = [source].[AuthorName]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Body], [CreationDate], [NewsId],[AuthorName]) 
	VALUES ([source].[Id], [source].[Body], [source].[CreationDate],[source].[NewsId],[source].[AuthorName]);

SET IDENTITY_INSERT dbo.Comments OFF
GO


GO

GO
DECLARE @VarDecimalSupported AS BIT;

SELECT @VarDecimalSupported = 0;

IF ((ServerProperty(N'EngineEdition') = 3)
    AND (((@@microsoftversion / power(2, 24) = 9)
          AND (@@microsoftversion & 0xffff >= 3024))
         OR ((@@microsoftversion / power(2, 24) = 10)
             AND (@@microsoftversion & 0xffff >= 1600))))
    SELECT @VarDecimalSupported = 1;

IF (@VarDecimalSupported > 0)
    BEGIN
        EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
    END


GO
PRINT N'Обновление завершено.';


GO
