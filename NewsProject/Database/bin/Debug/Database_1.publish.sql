﻿/*
Скрипт развертывания для NewsDatabase

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "NewsDatabase"
:setvar DefaultFilePrefix "NewsDatabase"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET IDENTITY_INSERT dbo.News ON

DECLARE @News TABLE(
	[Id] INT NOT NULL, 
    [Head] NVARCHAR(255) NOT NULL, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL 
)


INSERT INTO @News ([Id], [Head], [Body], [CreationDate]) VALUES
(1,'Валентина Терешкова приезжала в Тольятти','Как сообщает корреспондент TLTgorod, сегодня, 11 сентября, в школе № 49 состоялась торжественная церемония открытия мемориала в честь основателя российской космонавтики Сергея Павловича Королева. На этот праздник собрались все ученики, несмотря на выходной день. В парадной форме, с обилием воздушных шаров дети с нетерпением ждали, когда же они увидят первую женщину-космонавта на Земле – Валентину Терешкову, женщину, которая совершила космический полёт в одиночку.
- Для меня особая честь и волнение, что я среди вас, школьников. Потому что ваша школа носит великое имя Сергея Павловича Королева, - сказала на торжественной церемонии Валентина Терешкова - Мы всегда, всю свою жизнь, помним нашего учителя и наставника, человека который посвятил свою жизнь созданию ракетно-космической техники. Каждого из нас он отбирал, беседовал с нами. Пройти этот "рентген", как мы называли глаза и сами вопросы Сергея Павловича, было довольно сложно. Мы любили его по-настоящему. С его именем мы летели в космос, с его лучистыми глазами мы встречались на Земле, потому что он не только провожал нас в космос, но и поддерживал связь с космонавтами. Глядя на вас, я радуюсь тому, что все такие счастливые, красиво одетые. Невольно вспоминается военное время, потому что у нас не было красивых платьев и форм. Но мы понимали, что мы должны учиться и приносить пользу своей стране. Я хочу попросить, чтобы из стен этой школы вышли новые инженеры, конструкторы, чтобы вы всегда помнили и продолжали великое дело, начатое Сергеем Павловичем Королевым – изучение и полеты в космос. Надеюсь, что среди вас будет немало любителей космонавтики.','11.09.2016'),
(2,'В самом разгаре курсы EPAM','Спешим поделиться радостной новостью - мы открыли EPAM Тренинг-центр в Тольятти. 
На этой неделе стартовали курсы по направлениям: .NET, Java и Тестирование ПО. Студенты познакомились с тольяттинским офисом и учебной лабораторией, в которой уже начали заниматься. Так же получили приятные и полезные подарки, узнали много интересной информации и услышали напутственные слова от преподавателей. 
Желаем плодотворного обучения!','28.10.2016'),
(3,'Вторая Земля у нас под боком','Проксима Центавра находится от нас по космическим меркам совсем близко, на расстоянии всего 4,25 световых лет. Что касается открытой экзопланеты, то ее масса составляет около 1,3 земной. Год на ней длится 11,2 суток, средняя температура поверхности близка к нулю градусов Цельсия. По целому ряду признаков планета отвечает критериям, подходящим для возникновения жизни. Если бы не одно но... Непонятно, есть ли у нее атмосфера. Дело в том, что звезда Проксима Центавра выдает рентгеновские вспышки в 400 раз более мощные, чем получает наша Земля. Из-за этого атмосфера на экзопланете может просто улетучиться. Впрочем, ученые не столь пессимистичны. Они считают, что все зависит от того, как и когда сформировалась экзопланета. И тут возможные варианты, которые могут благоприятствовать появлению жизни. Так, если у нее есть собственное магнитное поле, то воздействие радиации может оказаться не столь сильным.','25.08.16')



MERGE INTO [News] AS [target]
USING @News AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Head]	= [source].[Head],
			[target].[Body] = [source].[Body],
			[target].[CreationDate] = [source].[CreationDate]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Head], [Body], [CreationDate]) 
	VALUES ([source].[Id], [source].[Head], [source].[Body],[source].[CreationDate]);

SET IDENTITY_INSERT dbo.News OFF
GO


GO

GO
PRINT N'Обновление завершено.';


GO
