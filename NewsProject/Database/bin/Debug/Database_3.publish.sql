﻿/*
Скрипт развертывания для NewsDatabase

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "NewsDatabase"
:setvar DefaultFilePrefix "NewsDatabase"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET IDENTITY_INSERT dbo.News ON

DECLARE @News TABLE(
	[Id] INT NOT NULL, 
    [Head] NVARCHAR(255) NOT NULL, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL 
)


INSERT INTO @News ([Id], [Head], [Body], [CreationDate]) VALUES
(1,'Валентина Терешкова приезжала в Тольятти','Как сообщает ктики.','11.09.2016'),
(2,'В самом разгаре курсы EPAM','Спешим поделиться радостной новостью - мы открыли EPAM Тренинг-центр в Тольятти. Плодотворного обучения!','28.10.2016'),
(3,'Вторая Земля у нас под боком','Проксима Центавра находится от нас  не столь сильным.','25.08.16')



MERGE INTO [News] AS [target]
USING @News AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Head]	= [source].[Head],
			[target].[Body] = [source].[Body],
			[target].[CreationDate] = [source].[CreationDate]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Head], [Body], [CreationDate]) 
	VALUES ([source].[Id], [source].[Head], [source].[Body],[source].[CreationDate]);

SET IDENTITY_INSERT dbo.News OFF
GO


GO

GO
PRINT N'Обновление завершено.';


GO
