﻿using NewsProject.DAL.Interfaces;
using NewsProject.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Data.Entity;
using System;

namespace UnitTests
{
    [TestClass]
    public class CommentsRepositoryTests
    {
        [TestMethod]
        // тестирование CommentsNewsRepository
        // метод:        public void Update(News item)
        public void CommentsRepo_UpdateTest()
        {
            var testAuthor = "Изменено тестом 366";
            var item = new Comments() { AuthorName="", Body="", CreationDate=DateTime.Now , NewsId=1 };

            var db = new NewsDatabaseEntities();
            db.Comments.Add(item);
            db.SaveChanges();
            item.AuthorName = testAuthor;
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            var resStandart = db.Comments.Where(c => c.AuthorName == testAuthor).FirstOrDefault();
            var standartResFlag = resStandart != null;
            db.Comments.Remove(item);
            db.SaveChanges();

            var item2 = new Comments() { AuthorName = "", Body = "", CreationDate = DateTime.Now, NewsId = 1 };

            var comments = new CommentsRepository(db);
            comments.Create(item2);
            comments.Save();
            item2.AuthorName = testAuthor;
            comments.Update(item2);
            comments.Save();
            var resRepo = comments.Find(c => c.AuthorName == testAuthor).FirstOrDefault();
            var repoResFlag = resStandart != null;
            comments.Delete(item2.Id);
            comments.Save();

            Assert.AreEqual(standartResFlag, repoResFlag);
        }

        [TestMethod]
        // тестирование CommentsNewsRepository
        // метод:           public IEnumerable<News> GetAll()
        public void CommentsRepo_GetAllTest()
        {
            var db = new NewsDatabaseEntities();
            var standartGetAll = db.Comments;

            var comments = new CommentsRepository(db);
            var repoGetAll = comments.GetAll();

            Assert.AreEqual(standartGetAll.Count(), repoGetAll.Count());
        }

        [TestMethod]
        // тестирование CommentsNewsRepository
        // метод:      public IEnumerable<News> Find(Func<News, bool> predicate)
        public void CommentsRepo_FindTest()
        {
            var db = new NewsDatabaseEntities();
            var standartSearch = db.Comments.Where(c => c.Id == 1).FirstOrDefault();

            var comments = new CommentsRepository(db);
            var repoSearch = comments.Find(c => c.Id == 1).FirstOrDefault();

            Assert.AreEqual(standartSearch.AuthorName, repoSearch.AuthorName);
        }

        [TestMethod]
        // тестирование CommentsNewsRepository
        // метод:       public News Get(int id)
        public void CommentsRepo_GetTest()
        {
            var db = new NewsDatabaseEntities();
            var standartGet = db.Comments.Find(1);

            var comments = new CommentsRepository(db);
            var repoGet = comments.Get(1);

            Assert.AreEqual(standartGet.AuthorName, repoGet.AuthorName);
        }


        [TestMethod]
        // тестирование CommentsRepository
        // метод:   public void Create(Comments item)
        public void CommentsRepo_CreateTest()
        {
            //arrange
            var db = new NewsDatabaseEntities();
            var comments = new CommentsRepository(db);

            var expectedBody = "хорошая новость";

            var newComment = new Comments() { AuthorName = "Сергей", Body = expectedBody, CreationDate = DateTime.Now, NewsId = 1 };

            //act   
            comments.Create(newComment);
            comments.Save();

            var result = db.Comments.Find(newComment.Id);

            // assert
            Assert.AreEqual(expectedBody, result.Body);

            db.Comments.Remove(result);
            db.SaveChanges();
        }

        [TestMethod]
        // тестирование CommentsRepository
        // метод:  public void Delete(int id)
        public void CommentsRepo_DeleteTest()
        {
            //arrange
            var db = new NewsDatabaseEntities();

            var expectedAuthor = "Тестовый Автор";


            db.Comments.Add(new Comments { AuthorName = expectedAuthor, Body = "", CreationDate = DateTime.Now, NewsId = 1 });
            db.SaveChanges();

            var comments = new CommentsRepository(db);

            //act               
            var newComment = db.Comments.Where(c => (c.AuthorName == expectedAuthor)).FirstOrDefault();
            if (newComment != null)
            {
                int id = newComment.Id;
                comments.Delete(id);
                comments.Save();
                newComment = db.Comments.Where(c => (c.Id == newComment.Id)).FirstOrDefault();
                // assert
                Assert.AreEqual(null, newComment);
            }
            else
            {
                Assert.Fail("Искомый элемент не найден в таблице Comments");
            }
        }
    }
}
