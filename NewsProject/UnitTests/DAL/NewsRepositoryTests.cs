﻿using System;
using NewsProject.DAL.Interfaces;
using NewsProject.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Data.Entity;

namespace UnitTests
{
    [TestClass]
    public class NewsRepositoryTests
    {
        [TestMethod]
        // тестирование NewsRepository
        // метод:        public void Update(News item)
        public void NewsRepo_UpdateTest()
        {
            var testHead = "Изменено тестом 365";
            News item = new News() { Annotation = "Уникальная сказка о былых временах", Body = "Жил был в диком лесу старец", Comments = null, CreationDate = DateTime.Now, Head = "" };

            var db = new NewsDatabaseEntities();
            db.News.Add(item);
            db.SaveChanges();
            item.Head = testHead;
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            var resStandart = db.News.Where(c => c.Head == testHead).FirstOrDefault();
            var standartResFlag = resStandart != null;
            db.News.Remove(item);
            db.SaveChanges();

            var item2 = new News() { Annotation = "Уникальная сказка о былых временах", Body = "Жил был в диком лесу старец", Comments = null, CreationDate = DateTime.Now, Head = "" };

            var news = new NewsRepository(db);
            news.Create(item2);
            news.Save();
            item2.Head = testHead;
            news.Update(item2);
            news.Save();
            var resRepo = news.Find(c => c.Head == testHead).FirstOrDefault();
            var repoResFlag = resStandart != null;
            news.Delete(item2.Id);
            news.Save();

            Assert.AreEqual(standartResFlag, repoResFlag);
        }

        [TestMethod]
        // тестирование NewsRepository
        // метод:           public IEnumerable<News> GetAll()
        public void NewsRepo_GetAllTest()
        {
            var db = new NewsDatabaseEntities();
            var standartGetAll = db.News;

            var news = new NewsRepository(db);
            var repoGetAll = news.GetAll();

            Assert.AreEqual(standartGetAll.Count(), repoGetAll.Count());
        }

        [TestMethod]
        // тестирование NewsRepository
        // метод:      public IEnumerable<News> Find(Func<News, bool> predicate)
        public void NewsRepo_FindTest()
        {
            var db = new NewsDatabaseEntities();
            var standartSearch = db.News.Where(c => c.Id == 1).FirstOrDefault();

            var news = new NewsRepository(db);
            var repoSearch = news.Find(c => c.Id == 1).FirstOrDefault();

            Assert.AreEqual(standartSearch.Head, repoSearch.Head);
        }

        [TestMethod]
        // тестирование NewsRepository
        // метод:       public News Get(int id)
        public void NewsRepo_GetTest()
        {
            var db = new NewsDatabaseEntities();
            var standartGet = db.News.Find(1);

            var news = new NewsRepository(db);
            var repoGet = news.Get(1);

            Assert.AreEqual(standartGet.Head, repoGet.Head);
        }
        
        [TestMethod]
        // тестирование NewsRepository
        // метод:   public void Create(News item)
        public void NewsRepo_CreateTest()
        {
            //arrange
            var db = new NewsDatabaseEntities();
            var news = new NewsRepository(db);

            var expectedHead = "Сказка";
            var freshNew = new News() { Annotation = "Уникальная сказка о былых временах", Body = "Жил был в диком лесу старец", Comments = null, CreationDate = DateTime.Now, Head = expectedHead };

            //act   
            news.Create(freshNew);
            db.SaveChanges();

            var result = db.News.Find(freshNew.Id);

            // assert
            Assert.AreEqual(expectedHead, result.Head);
            db.News.Remove(result);
            db.SaveChanges();
        }

        [TestMethod]
        // тестирование NewsRepository
        // метод:   public void Delete(News item)
        public void NewsRepo_DeleteTest()
        {
            //arrange
            var db = new NewsDatabaseEntities();

            var expectedHead = "Сказка";
            NewsRepository news = new NewsRepository(db);


            db.News.Add(new News { Body = "", CreationDate = DateTime.Now, Annotation="", Head= expectedHead});
            db.SaveChanges();

            //act   
            var freshNew = db.News.Where(c => (c.Head == expectedHead)).FirstOrDefault();

            if (freshNew != null)
            {
                int id = freshNew.Id;
                news.Delete(id);
                db.SaveChanges();
                freshNew = db.News.Where(c => (c.Id == freshNew.Id)).FirstOrDefault();

                // assert
                Assert.AreEqual(null, freshNew);
            }
            else
            {
                Assert.Fail("Искомый элемент не найден в таблице News");
            }
        }

        
    }
}
