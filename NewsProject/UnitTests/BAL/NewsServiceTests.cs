﻿using System;
using NewsProject.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using NewsProject.BAL.Services;
using NewsProject.BAL;
using System.Collections.Generic;
using System.Data.Entity;
using NewsProject.BAL.Interfaces;
using NewsProject.DAL.Interfaces;

namespace UnitTests
{
    [TestClass]
    public class NewsServiceTests
    {
        private static readonly IMyDbcontext Db = new NewsDatabaseEntities();
        private static readonly IRepository<News> RepoNews = new NewsRepository(Db);
        private static readonly INewsService NewsService = new NewsService(Db, RepoNews);


        [TestMethod]
        // тестирование сервиса News
        // метод:     public List<NewsDTO> GetAllNews()
        public void NewsService_GetAllNews()
        {
            //Arrange
            //Act
            var standartCnt = Db.News.Count();
            var newsServiceCnt = NewsService.GetAllNews().Count();

            //Assert
            Assert.AreEqual(standartCnt, newsServiceCnt);
        }

        [TestMethod]
        // тестирование сервиса News
        // метод: public void AddNews(NewsDTO item)
        public void NewsService_AddNews()
        {
            //Arrange
            const string testHead = "тестовый Заголовок";
            var item = new NewsDto { Annotation = "", Body = "", CreationDate = DateTime.Now, Head = testHead, Comments = new List<CommentsDto>() };

            //Act
            NewsService.AddNews(item);

            var result = Db.News.FirstOrDefault(c => c.Head == testHead);
            Db.News.Remove(result);
            Db.SaveChanges();

            //Assert
            Assert.AreEqual(testHead, result.Head);

        }

        [TestMethod]
        // тестирование сервиса News
        // метод: public NewsDTO FindNews(int id)
        public void NewsService_FindNews()
        {

            //Arrange
            //Act
            var resStandart = Db.News.Find(1).Head;
            var resultService = NewsService.FindNews(1).Head;

            //Arrange
            Assert.AreEqual(resStandart, resultService);
        }


        [TestMethod]
        // тестирование сервиса News
        // метод:  public ICollection<CommentsDto> GetNewsComments(int id)
        public void NewsService_GetNewsComments()
        {
            //Arrange
            //Act
            var resStandart = Db.News.Count();
            var resultService = NewsService.GetAllNews().Count();

            //Assert
            Assert.AreEqual(resStandart, resultService);
        }


        [TestMethod]
        // тестирование сервиса News
        // метод:  public void UpdateNews(NewsDTO item)
        public void NewsService_UpdateNews()
        {
            //Arrange
            const string testHead = "Изменено тестом 365";
            var item = new News() { Annotation = "Уникальная сказка о былых временах", Body = "Жил был в диком лесу старец", Comments = new List<Comments>(), CreationDate = DateTime.Now, Head = "" };
          
            //Act
            Db.News.Add(item);
            Db.SaveChanges();
            item.Head = testHead;
            Db.Entry(item).State = EntityState.Modified;
            Db.SaveChanges();
            var resStandart = Db.News.FirstOrDefault(c => c.Head == testHead);
            var standartResFlag = resStandart != null;


            Db.News.Remove(Db.News.First(c => c.Head == item.Head));
            Db.SaveChanges();

            var item2 = new NewsDto() { Annotation = "Уникальная сказка о былых временах", Body = "Жил был в диком лесу старец", Comments = new List<CommentsDto>(), CreationDate = DateTime.Now, Head = "" };

            
            NewsService.AddNews(item2);

            item2 = NewsService.GetAllNews().First(c => c.Annotation == item2.Annotation);
            item2.Head = testHead;
            NewsService.UpdateNews(item2);

            var resSrvc = NewsService.GetAllNews().First(c => c.Head == testHead);
            var srvcResFlag = resSrvc != null;

            NewsService.DeleteNews(Db.News.First(c => c.Head == item2.Head).MapNewsToNewsDto());

            //Assert
            Assert.AreEqual(standartResFlag, srvcResFlag);
        }


        [TestMethod]
        // тестирование сервиса News
        // метод:  public void DeleteNews(NewsDTO item)
        public void NewsService_DeleteNews()
        {
            //arrange    
            var expectedHead = "Сказка";


            //act 
            Db.News.Add(new News { Body = "", CreationDate = DateTime.Now, Annotation = "", Head = expectedHead, Comments = new List<Comments>() });
            Db.SaveChanges();
              
            var freshNew = Db.News.First(c => (c.Head == expectedHead)).MapNewsToNewsDto();

            if (freshNew != null)
            {
                int id = freshNew.Id;
                NewsService.DeleteNews(freshNew);

                // assert
                Assert.AreEqual(null, Db.News.FirstOrDefault(c => (c.Id == id)));
            }
            else
            {
                Assert.Fail("Искомый элемент не найден в таблице News");
            }
        }
    }
}

